The Daily module for Drupal.

Please see the INSTALL document for installation and upgrade guidelines.

This module, and all files distributed with it, is released under the
GNU General Public License, version 2 or (at your option) any later version.

Once you have installed this module (see INSTALL), you should read the
included help information under the Administer/Help menu.

A short description of this module:

When enabled, a new node type called Daily Container is created. This node
type is meant for content which is renewed daily, but of which old versions
should remain accessible. A good example is a daily comic strip.

A Daily Container node has variable content, which is typically refreshed
upto once a day. It can display the newest, oldest or any other well-defined
or random node of a range of daily content. Any node of any type can be
associated with such a range of dated content.

To determine which nodes should go together, a taxonomy (also known
as vocabulary) is used. All nodes which have the same daily vocabulary term
belong together. These nodes will also display a calendar when viewed, to
make it possible to select other nodes from the same range which are older
or newer.

Nodes which are daily content have a date associated with them. This may
be a date in the past or future. In the latter case, optionally the node
is only made accessible on that future date.
