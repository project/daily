/*
    daily.js - Part of the daily module for Drupal
    Copyright (c) 2007-2008  Frodo Looijaard <drupal@frodo.looijaard.name>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// Register callbacks
$(document).ready(function() {
  // If the daily date is changed, check the enable checkbox.
  // Note that all date selection dropdowns have the daily-date class.
  $(".daily-date").change(function() {
    $("input.daily-enable").check('on');
  } );
} );

// Stolen from the 'Getting Started with jQuery' tutorial from
// jquery.bassistance.de
// Check, uncheck or toggle one or more checkboxes
// mode may be 'on' (default), 'off' or 'toggle'
$.fn.check = function(mode) {
  var mode = mode || 'on';
  return this.each(function() {
    switch(mode) {
      case 'on':
        this.checked = true;
        break;
      case 'off':
        this.checked = false;
      break;
        case 'toggle':
        this.checked = !this.checked;
      break;
    }
  });
};

// vim:sw=2:ft=javascript:
