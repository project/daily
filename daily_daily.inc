<?php

/*
    daily_daily.inc - Part of the daily module for Drupal
    Copyright (c) 2004-2008  Frodo Looijaard <drupal@frodo.looijaard.name>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/** @file 
 * All code which adds the daily section to all node types.
 *
 * @ingroup daily
 */

include_once "daily_container.inc";
include_once "daily_data.inc";
include_once "daily_aux.inc";

/** @defgroup DailyDaily Daily content insertion
 * Functions that add daily content to other node types.
 *
 * @{
 */

/** @ingroup DailyHooks
 * Implementation of hook_nodeapi(): Act on nodes defined by other modules.
 *
 * * load: Add daily_date and daily_autopublish fields if present.
 * * validate: Validate the entered date and term.
 * * update: Handle revision reverts, avoid double dates. Insert data
 *   into the daily table (and delete old data).
 * * insert: Insert data into the daily table (and delete old data).
 * * delete: Remove data from the daily table.
 * * delete_revision: Remove data from the daily tables.
 * * view: Add the calendar.
 * * presave: Publish/unpublish if autopublish is choosen.
 */
function daily_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'load': 
      # Try to find a matching row in the 'daily' table. Note that we
      # rename the fields to use a daily_ prefix.
      $data = db_fetch_object(db_query("SELECT date,autopublish FROM {daily} WHERE vid = %d", $node->vid));
      if (is_object($data)) {
        return array("daily_date" => _daily_unpack_date($data->date), "daily_autopublish" => $data->autopublish);
      }
      break;
    case 'validate':
      if (isset($node->daily_enable) and $node->daily_enable) {
        $date = $node->daily_date;
        if ($date["year"] < 1970 or $date["year"] > 2050) {
          form_set_error('daily_date', "Invalid year selected");
        }
        elseif ($date["month"] < 1 or $date["month"] > 12) {
            form_set_error('daily_date', "Invalid month selected (form submission problem?)");
        }
        elseif ($date["day"] < 1 or $date["day"] > _daily_days_per_month($date["month"], $date["year"])) {
          form_set_error('daily_date', "Invalid day of month selected");
        }
        $tids = _daily_find_our_tids($node->taxonomy);
        if (count($tids) == 0) {
          $vid = variable_get("daily_vocabulary", 0);
          form_set_error('taxonomy]['.$vid, _daily_form_error_no_tid());
        }
        elseif (count($tids) > 1) {
          $vid = variable_get("daily_vocabulary", 0);
          form_set_error('taxonomy]['.$vid, _daily_form_error_multiple_select_vocabulary($vid));
        }
        /* Check for a duplicate. It is a duplicate if it has the same date and
         * a different node id, provided it has the same taxonomy term id */
        elseif (db_result(db_query("SELECT * FROM {daily} d INNER JOIN {node} n ON n.vid = d.vid INNER JOIN {term_node} t ON n.vid = t.vid WHERE d.date = '%s' AND t.tid = %d AND n.nid <> %d", _daily_pack_date($node->daily_date), $tids[0], $node->nid))) {
          form_set_error('daily_date', _daily_form_error_duplicate_date());
        }
      }
      break;
    case 'update':
      // Check for a revision revert. Note that on a submit, old_vid may be
      // set too, hence the check for node->op.
      if ($node->old_vid and !isset($node->op)) {
        // If the current revision is a daily item, but the revision into
        // which we reverty is not, the daily_* fields will be set with the
        // current revision values (because they are not loaded when the
        // old revision was loaded, and unset values are overwritten).
        if (! db_result(db_query("SELECT COUNT(*) FROM {daily} WHERE vid = %d", $node->old_vid))) {
          unset($node->daily_date);
          unset($node->daily_autopublish);
        }

        if (isset($node->daily_date)) {
          $tids = _daily_find_our_tids($node->taxonomy, 'revert');


          // Revision reverts do not go through any *_validate function. We 
          // need to be careful not to have double dates. So we check here
          // for it and fix it. With a warning.
          $query = db_query("SELECT n.title,d.nid FROM {daily} d INNER JOIN {node} n ON n.vid = d.vid INNER JOIN {term_node} t ON t.vid = d.vid WHERE t.tid = %d AND n.nid != %d AND d.date = '%s'", $tids[0], $node->nid, _daily_pack_date($node->daily_date));
          if ($problem_node = db_fetch_object($query)) {
            $olddate = $node->daily_date;
            do {
              _daily_tomorrow($node->daily_date);
              $query = db_query("SELECT n.title,d.nid FROM {daily} d INNER JOIN {node} n ON n.vid = d.vid INNER JOIN {term_node} t ON t.vid = d.vid WHERE t.tid = %d AND n.nid != %d AND d.date = '%s'", $tids[0], $node->nid, _daily_pack_date($node->daily_date));
            } while (db_fetch_object($query));
          }
          if (isset($olddate)) {
            drupal_set_message(t('WARNING: by reverting to this node revision, you would have introduced duplicate publication dates. To prevent this, the daily date of this node has been changed from %olddate to %newdate. The node which caused this problem is !probnode', array('!probnode' => l($problem_node->title, 'node/'.$problem_node->nid.'/edit'), '%olddate'=> _daily_format_date($olddate), '%newdate' => _daily_format_date($node->daily_date))), 'error');
          }
          db_query("INSERT INTO {daily} (vid,nid,date,autopublish) VALUES (%d,%d,'%s','%s')", $node->vid, $node->nid, _daily_pack_date($node->daily_date), $node->daily_autopublish);
        }
        break;
      }
    # Fallthrough from 'update' in case no revision revert is going on
    case 'insert': 
      if (isset($node->daily_enable)) {
        db_query("DELETE FROM {daily} WHERE vid = %d", $node->vid);
        if ($node->daily_enable) {
          db_query("INSERT INTO {daily} (nid,vid,date,autopublish) VALUES (%d,%d,'%s','%s')", $node->nid, $node->vid, _daily_pack_date($node->daily_date), $node->daily_autopublish);
        }
      }
      break;
    case 'delete':
      if (isset($node->daily_date)) {
        db_query("DELETE FROM {daily} WHERE nid = %d", $node->nid);
      }
      break;
    case 'delete revision':
      if (isset($node->daily_date)) {
        db_query("DELETE FROM {daily} WHERE vid = %d", $node->vid);
      }
      # We need the container check too here...
      if ($node->type == "daily_container") {
        _daily_delete_revision($node);
      }
      break;
    case 'view':
      if (isset($node->daily_date) and (!isset($node->daily_enable) or $node->daily_enable)) {
        _daily_css();
        # When previewing, the taxonomy array has a different format...
        $tids = _daily_find_our_tids($node->taxonomy, ($node->op == 'Preview')?'edit':'view');
        $node->content['daily'] = array(
          /* TODO: Weight */
          '#value' => theme("daily_calendar_month", _daily_calendar_month_data($tids[0], $node->daily_date, $node->nid))
        );
      }
      break;
    case 'presave':
      if (isset($node->daily_enable) and $node->daily_enable and $node->daily_autopublish) {
        $node->status = _daily_pack_date($node->daily_date) <= _daily_pack_date(_daily_today())?1:0;
      }
      break;
  }
}

/** @ingroup DailyHooks
 * Implementation of hook_form_alter(): Change a form.
 *
 * The *_node_form is changed if this node type can be daily content.
 * This is determined by checking whether it can have terms in the daily
 * vocabulary.
 */
function daily_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['type']) && ($form['type']['#value'] .'_node_form' == $form_id) && ($vid = variable_get("daily_vocabulary", 0)) && ($voc = taxonomy_vocabulary_load($vid)) && (in_array($form['type']['#value'], $voc->nodes))) { 

    $node = $form['#node'];

    $form['daily'] = array(
      '#type' => 'fieldset',
      '#title' => t('Daily content'),
      '#collapsible' => TRUE,
      '#collapsed' => isset($node->daily_enable)?!($node->daily_enable):!isset($node->daily_date),
      '#access' => 1,
      '#weight' => 30,
    );

    $form['daily']['daily_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t("Enable as daily content"),
      '#return_value' => 1,
      '#default_value' => isset($node->daily_enable)?$node->daily_enable:isset($node->daily_date),
      '#description' => t("Turn this node into daily content. You need to choose a term in the daily vocabulary %voc if you enable this.", array('%voc' => $voc->name)),
      '#attributes' => array("class" => "daily-enable"),
    );

    /* TODO: Range checking for dates */
    $form['daily']['daily_date'] = array(
      '#type' => 'date',
      '#title' => t("Date"),
      '#default_value' => $node->daily_date,
      '#description' => t("The publication date"),
      '#required' => TRUE,
      '#attributes' => array("class" => "daily-date"),
    );

    $form['daily']['daily_autopublish'] = array(
      '#type' => 'checkbox',
      '#title' => t("Publish automatically"),
      '#return_value' => 1,
      '#default_value' => (isset($node->daily_autopublish)?$node->daily_autopublish:variable_get("daily_default_autopublish", 1)),
      '#description' => t("Publish this note automatically on the selected date"),
    );
  }

  _daily_js();
}

/** @}
 */

# vim:sw=2:ft=php:et
