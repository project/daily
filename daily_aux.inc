<?php

/*
    daily_aux.inc - Part of the daily module for Drupal
    Copyright (c) 2004-2008  Frodo Looijaard <drupal@frodo.looijaard.name>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/** @file
 * Small, auxiliary functions called by other parts of the daily module:
 * date related functions and error messages.
 *
 * @ingroup daily
 */

/**************************
 * DATE RELATED FUNCTIONS *
 **************************/

/** @defgroup DailyDate Date related functions
 * Several functions which manipulate dates.
 *
 * Two date representations are used: a date string, of the form YYYYMMDD, 
 * and a date array, which has fields day, month and year.
 *
 * @{
 */

/**
 * Pack a date array into a date string.
 *
 * @param $date
 *   The date array to convert. A date array has fields day, month and year.
 * @return
 *   The resulting date string. A date string is of the form YYYYMMDD.
 */
function _daily_pack_date($date) {
  return sprintf("%04d%02d%02d", $date["year"], $date["month"], $date["day"]);
}

/**
 * Unpack a date string into a date array.
 * 
 * @param $date
 *   The date string to convert. A date string is of the form YYYYMMDD.
 * @return The resulting date array. A date array has fields day, month and year.
 */
function _daily_unpack_date($date) {
  $result["year"] = (int) drupal_substr($date, 0, 4);
  $result["month"] = (int) drupal_substr($date, 4, 2);
  $result["day"] = (int) drupal_substr($date, 6, 2);
  return $result;
}

/**
 * Get the current time in a date array.
 *
 * The date array is cached, so we only have to compute it once.
 * 
 * @return 
 *   A date array with today's date. A date array has fields year, month and day.
 */
function _daily_today() {
  static $today;
  if (!$today) {
    $current_time = localtime(time(), True);
    $today["year"] = $current_time['tm_year'] + 1900;
    $today["month"] = $current_time['tm_mon'] + 1;
    $today["day"] = $current_time['tm_mday'];
  }
  return $today;
}


/**
 * Get the number of days of a certain month.
 *
 * Both $month and $year are needed to account for leap years.
 *
 * @param $month 
 *   The month (1 = Jan, 12 = Dec)
 * @param $year
 *   The year  (for example, 2000)
 * @return
 *   The number of days is this particular month.
 */
function _daily_days_per_month($month, $year) {
  switch ($month) {
    case 1: case 3: case 5: case 7: case 8: case 10: case 12:
      return 31;
    case 4: case 6: case 9: case 11: default:
      return 30;
    case 2:
      if ($year % 4 == 0) {
        return 29;
      }
      else {
        return 28;
      }
  }
}

/** Format a date array for output.
 *
 * @param $date
 *   The date array. A date array has fields year, month and day.
 * @param $format 
 *   The date format to use. Supported formats are small, medium and large.
 *   These are the formats as used by the Drupal function format_date, except
 *   that only the date part is outputed.
 * @return 
 *   A string containing the formatted date.
 */
function _daily_format_date($date, $format = "large") {
  if ($format != "large" and $format != "medium" and $format != "small") {
    $format = "large";
  }
  return preg_replace('/ *-? *\S*$/', '', format_date(mktime(12, 0, 0, $date['month'], $date['day'], $date['year']), $format));
}

/** Update a date array to tomorrow.
 * 
 * @param $date
 *   The date to update (passed by reference). A date array has fields year,
 *   month and day.
 */
function _daily_tomorrow(&$date) {
  $date["day"] ++;
  if (_daily_days_per_month($date["month"], $date["year"]) < $date["day"]) {
    $date["day"] = 1;
    $date["month"] ++;
    if ($date["month"] = 13) {
      $date["month"] = 1;
      $date["year"]++;
    }
  }
}

/** @}
 */


/******************
 * ERROR MESSAGES *
 ******************/

/** @defgroup DailyErrorMessages Error messages
 * Functions that return HTML-formatted error messages.
 *
 * @{
 */

/**
 * Return an HTMLized error reflecting the taxonomy module is not enabled.
 *
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_no_taxonomy() {
  return t("You do not seem to have the taxonomy module enabled. This is a requirement. You can enable it in !admin.", array('!admin' => l(t("the modules administration page"), "admin/modules")));
}

/**
 * Return an HTMLized error reflecting you have no daily taxonomy selected.
 *
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_no_vocabulary() {
  return t("You do not seem to have a vocabulary assigned to daily items. This is a requirement. You can enable it in !admin.", array('!admin' => l(t("the daily module administration page"), "admin/settings/daily")));
}

/**
 * Return an HTMLized error reflecting your daily vocabulary has multiple
 * select enabled.
 *
 * @param $vid
 *   The vocabulary id for which the error message should be generated.
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_multiple_select_vocabulary($vid) {
  return t("Nodes marked as daily content may only have one term selected in the vocabulary assigned to daily items. You should remove the option %mulsel in the !vocpage.", array('%mulsel'=> t('Multiple select'), '!vocpage' => l(t("the edit page for the daily vocabulary"), "admin/content/taxonomy/edit/vocabulary/". $vid)));
}

/**
 * Return an HTMLized error reflecting a daily container may not be included
 * within a daily container.
 *
 * @param $vid
 *   The vocabulary id for which the error message should be generated.
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_recursive_container($vid) {
  return t("Daily containers may not select terms in the daily vocabulary. You should deselect the %container checkbox within the %types list in the !vocpage.", array('%container'=> t('Daily Container'), '%types' => t('Types'), '!vocpage' => l(t("the edit page for the daily vocabulary"), "admin/content/taxonomy/edit/vocabulary/". $vid))) ;
}

/**
 * Return an HTMLized error reflecting you must select a daily taxonomy term.
 *
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_no_tid() {
  return t("You have not selected a term in the taxonomy you assigned to daily items.");
}

/**
 * Return an HTMLized error reflecting you have to enter a proper number.
 *
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_correct_nr() {
  return t("Please enter a whole number (positive or negative).");
}


/**
 * Return an HTMLized error reflecting a daily item with this date already
 * exists.
 *
 * @return 
 *   A string containing the HTML error.
 */
function _daily_form_error_duplicate_date() {
  return t("Duplicate date: you have to enter a unique date.");
}

/**
 * Return an HTMLized error reflecting no nodes of this type have been defined> 
 *
 * @return
 *   A string containing the HTML error.
 */
function _daily_form_error_no_nodes_found($tid) {
  $term = taxonomy_get_term($tid);
  return t("There are no nodes defined yet with the taxonomy term %term", array('%term' => $term->name));
}


/** @}
 */


# vim:sw=2:ft=php:et

?>
