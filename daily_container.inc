<?php

/*
    daily_container.inc - Part of the daily module for Drupal
    Copyright (c) 2004-2008  Frodo Looijaard <drupal@frodo.looijaard.name>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/** @file 
 * All code implementing the daily_container node type 
 *
 * @ingroup daily
 */

/** @defgroup DailyContainer Daily Container node type functions
 * All functions which define the node type Daily Container.
 *
 * @{
 */

/** @ingroup DailyHooks
 * Implementation of hook_node_info(): Node type information
 *
 * A new node type Daily Container is defined here.
 */
function daily_node_info() {
  return array("daily_container" => 
           array('name' => t("Daily container"),
                 'module' => 'daily',
                 'description' => t("A daily container is a node which displays daily changing content.")
                )
              );
}

/** @ingroup DailyHooks
 * Implementation of hook_form(): Build a node editing form for node type
 * Daily Container.
 */
function daily_form(&$node, &$param) {
  $type = node_get_types('type', $node);

  $vid = variable_get("daily_vocabulary", 0);
  $query = db_query("SELECT td.tid, td.name FROM {term_data} td WHERE td.vid = %d ORDER BY td.name", $vid);

  while ($db_record = db_fetch_object($query)) {
    $options[$db_record->tid] = $db_record->name;
  }

  # PHP5 complains if no options are present
  if (!count($options)) {
    $options[0] = '<none>';
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $node->title,
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => NULL,
    '#attributes' => NULL,
    '#required' => TRUE,
  );

  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t("Vocabulary term"),
    '#default_value' => $node->tid,
    '#options' => $options,
    '#description' => t("The daily item class"),
    '#extra' => 0,
    '#multiple' => FALSE,
    '#required' => TRUE,
  );

  $form["nr"] = array(
    '#type' => 'textfield',
    '#title' => t("Position"),
    '#default_value' => $node->nr,
    '#size' => 3,
    '#maxlength' => 5,
    '#description' => t("The position within the class. Use -1 for the last item, -2 for the last-but-one item, etc; use 1 for the first item, 2 for the second item, etc; use 0 for a random item refreshed once a day."),
    '#attributes' => NULL,
    '#required' => TRUE,
  );

  $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);

  return $form;
}

/** @ingroup DailyHooks
 * Implementation of hook_validate(): Verify a node editing form for nodetype
 * Daily Container.
 */
function daily_validate(&$node) {
  # If we have already form errors flagged, we will not do any further
  # checking
  if (form_get_errors()) {
    return;
  }

  $vid = variable_get("daily_vocabulary", 0);
  $term = module_invoke('taxonomy', 'get_term', $node->tid);
  if (!$node->tid or !$term or ($term->vid != $vid)) {
    form_set_error('tid', _daily_form_error_no_tid());
  }
  if ($node->nr !== (string) (int) $node->nr or $node->nr != floor($node->nr)) {
    form_set_error('nr', _daily_form_error_correct_nr());
  }

  $tids = _daily_find_our_tids($node->taxonomy);
  if (count($tids) != 0) {
    form_set_error('vocabulary]['. $vid, _daily_form_error_recursive_container($vid));
  }
}

/** @ingroup DailyHooks
 * Implementation of hook_prepare(): Prepare a node for editing
 *
 * This is always called for Daily Container nodes when starting a new node,
 * when editing an existing node, when previewing a node and when submitting a
 * node.
 * 
 * We only do a few sanity checks here.
 */
function daily_prepare(&$node) {
  if (!module_exists("taxonomy")) {
    form_set_error('fatal',  _daily_form_error_no_taxonomy());
    return;
  }

  $vid = variable_get("daily_vocabulary", 0);
  if (!$vid) {
    form_set_error('fatal', _daily_form_error_no_vocabulary());
    return;
  }
}

/** @ingroup DailyHooks
 * Implementation of hook_insert(): Respond to node insertion for nodetype
 * Daily Container.
 *
 * Updates the database table {daily_container}.
 */
function daily_insert($node) {
  db_query("INSERT INTO {daily_container} (vid, nid, tid, nr) VALUES (%d, %d, %d, %d)", $node->vid, $node->nid, $node->tid, $node->nr);
}

/** @ingroup DailyHooks
 * Implementation of hook_update(): Respond to node updating for nodetype
 * Daily Container.
 *
 * Updates the database table {daily_container}
 */
function daily_update($node) {
  if ($node->old_vid) {
    /* Insert a new revision */
    db_query("INSERT INTO {daily_container} (vid, nid, tid, nr) VALUES (%d, %d, %d, %d)", $node->vid, $node->nid, $node->tid, $node->nr);
  } 
  else {
    /* Just update the node */
    db_query("UPDATE {daily_container} SET tid=%d, nr=%d WHERE vid=%d", $node->tid, $node->nr, $node->vid);
  }
}


/** @ingroup DailyHooks
 * Implementation of hook_load(): Load additional node data for node type
 * Daily Container.
 */
function daily_load(&$node) {
  $data = db_fetch_object(db_query("SELECT tid, nr FROM {daily_container} WHERE vid = %d", $node->vid));
  return $data;
}

/** @ingroup DailyHooks
 * Implementation of hook_delete(): Respond to node deletion for node type
 * Daily Container.
 *
 * Updates the database table {daily_container}
 */
function daily_delete(&$node) {
  // We use the nid field here because we need to remove all revisions
  // and there is no way to get the revision id's.
  // Hopefully, this will change in the future.
  db_query("DELETE FROM {daily_container} WHERE nid = %d", $node->nid);
}

/** @ingroup DailyHooks
 * Implementation of hook_view(): Add extra fields to display.
 *
 * We add the daily node we need this time.
 */
function daily_view($node, $teaser = FALSE, $page = FALSE) { 
  $node = node_prepare($node, $teaser);
  
  $contained_node = _daily_node_by_tid($node->tid, $node->nr);
  if (is_object($contained_node)) {
    $node->content['daily_container'] = array(
      '#prefix' => '<div class="daily-contained">',
      '#value' => node_view(_daily_node_by_tid($node->tid, $node->nr), $teaser, FALSE),
      '#suffix' => '</div>',
      '#weight' => 1,
    );
  }
  else {
    # Usually, the calendar display will add the CSS.
    _daily_css();
    $node->content['daily_container'] = array(
      '#value' => '<div class="daily-empty">'. _daily_form_error_no_nodes_found($node->tid) .'</div>',
      '#weight' => 1,
    );
  }
      
  return $node;
}

/** @ingroup DailyHooks
 * Implementation of hook_access(): access restrictions for nodes
 */
function daily_access($op, $node) {
  global $user;

  switch ($op) {
    case 'create':
      return user_access('create daily containers');
      break;
    case 'update':
    case 'delete':
      return user_access('edit any daily containers') || (user_access('edit own daily containers') && $user->uid == $node->uid);
      break;
  }
}

/** 
 * This is called from daily_nodeapi on a 'delete revision' event, as there
 * is no normal hook.
 *
 * Removes a single revision.
 */
function _daily_delete_revision($node) {
  db_query("DELETE FROM {daily_container} WHERE vid = %d", $node->vid);
}

/** @}
 */


# vim:sw=2:ft=php:et
