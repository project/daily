<?php

/*
    daily_data.inc - Part of the daily module for Drupal
    Copyright (c) 2004-2008  Frodo Looijaard <drupal@frodo.looijaard.name>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/** @file 
 * Functions for linking Javascript and CSS files, for retrieving data and 
 * for theming.
 *
 * @ingroup daily
 */

/**************************
 * LINKING EXTERNAL FILES *
 **************************/

/** @defgroup DailyLinks Linking external files
 * Functions for creating links to JavaScript and CSS files.
 *
 * @{
 */

/**
 * Add a link to the daily CSS sheet.
 *
 * This function adds a link in the output HTML to the CSS sheet belonging to
 * the daily module. It makes sure the link is only added once.
 */
function _daily_css() {
  static $daily_css_added;
  if (!$daily_css_added) {
    drupal_add_css(drupal_get_path('module', 'daily') .'/daily.css');
    $daily_css_added = 1;
  }
}

/**
 * Add a link to the daily Javascript code.
 *
 * This function adds a link in the output HTML to the Javascript code 
 * belonging to the daily module. It makes sure the link is only added once.
 */
function _daily_js() {
  static $daily_js_added;
  if (!$daily_js_added) {
    drupal_add_js(drupal_get_path('module', 'daily') .'/daily.js');
    $daily_js_added = 1;
  }
}

/** @}
 */


/*******************
 * RETRIEVING DATA *
 *******************/

/** @defgroup DailyData Retrieving data
 * Functions that search data to retrieve needed information.
 *
 * @{
 */

/**
 * Find the taxonomy term id that belongs to the daily vocabulary.
 *
 * An array of $terms is filtered and only those terms that belong to the
 * daily vocabulary are returned.
 *
 * The format of the $terms array depends on the $kind parameter (below,
 * vid indicated a vocabulary id and tid a term id):
 * * edit:
 *   $terms is an array indexed by vid. Each element is either a single
 *   tid, or an array of tids.
 * * revert:
 *   $terms is an array of tids.
 * * view:
 *   $terms is an array of objects. Each object has at least a vid and a
 *   tid field.
 *   
 * @param $terms 
 *   An array of terms. The exact representation depends on the $kind
 *   parameter.
 * @param $kind 
 *   A string indicating the format of $tids. Valid values are edit, view and
 *   revert.
 * @return 
 *   An array of found tids.
 */
function _daily_find_our_tids($terms, $kind = 'edit') {
  $vid = variable_get("daily_vocabulary", 0);
  switch ($kind) {
    case 'view':
      $found = array();
      foreach ($terms as $term) {
        if ($term->vid == $vid) {
          $found[] = $term->tid;
        }
      }
      return $found;
      break;
    case 'edit':
      if (!$vid or !$terms or !array_key_exists($vid, $terms)) {
        return array();
      }
      elseif (is_array($terms[$vid])) {
        $found = $terms[$vid];   }
      else {
        $found = array($terms[$vid]);
      }
      $result = array();
      foreach ($found as $value) {
        if ($value) {
          $result[] = $value;
        }
      }
      return $result;
      break;
    case 'revert':
      $result = array();
      foreach ($terms as $tid) {
        $term = taxonomy_get_term($tid);
        if ($term->vid == $vid) {
          $result[] = $tid;
        }
      }
      return $result;
  }
}

/**
 * Retrieve the node of a daily item associated with a tid on a certain offset.
 *
 * This is the workhorse function that finds the node to display for a 
 * daily container.
 *
 * @param $tid 
 *   Taxonomy term identifier.
 * @param $number
 *   Number of the page to display. $number is negative to count
 *   from the end (-1=last), and positive to count from the start (1=first).
 *   If there are not enough daily items, the first respectively last node
 *   is returned. 0 means: get a random comic.
 * @return The fully filled node object found (or nothing if not found).
 */
function _daily_node_by_tid($tid, $number) {
  $sql_extra = (user_access('administer nodes') and $number != 0)?"":" AND n.status = 1";
  if ($tid != (string) (int) $tid or $tid != floor($tid)) {
    return;
  }
  if ($number != (string) (int) $number or $number != floor($number)) {
    return;
  }

  if (! $number) {
    return _daily_node_by_tid_random($tid);
  }

  $query = db_query("SELECT t.nid FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {daily} d ON n.vid = d.vid WHERE t.tid = %d". $sql_extra ." ORDER BY d.date %s LIMIT 1 OFFSET %d", $tid, ($number < 0?"DESC":""), abs($number) - 1);
  if (!($db_record = db_fetch_array($query))) {
    // If we are out of bound, return the first or last record.
    $query = db_query("SELECT t.nid FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {daily} d ON n.vid = d.vid WHERE t.tid = %d". $sql_extra ." ORDER BY d.date %s LIMIT 1 OFFSET %d", $tid, ($number > 0?"DESC":""));
    if (!($db_record = db_fetch_array($query))) {
      return;
    }
  }
  $node = node_load($db_record["nid"]);
  return $node;
}

/**
 * Retrieve the node of a daily item associated with a tid on a random offset.
 *
 * @param 
 *   $tid Taxonomy term identifier
 * @return 
 *   The fully filled node object found (or nothing if not found).
 */
function _daily_node_by_tid_random($tid) {
  // Check whether we have already one today which is still valid
  $db_record = db_fetch_object(db_query("SELECT dr.nid FROM {daily_random} dr INNER JOIN {node} n ON dr.nid = n.nid WHERE dr.tid = %d AND n.status = 1 AND dr.date = '%s'", $tid, _daily_pack_date(_daily_today())));

  if ($db_record) {
    $nid = $db_record->nid;
  } 
  else {
    // If we did not find one, generate a new one
    db_query("DELETE FROM {daily_random} WHERE tid = %d", $tid);
    $nr_of_rows = db_result(db_query("SELECT COUNT(*) FROM {term_node} t INNER JOIN {daily} d ON t.vid=d.vid INNER JOIN {node} n ON t.vid = n.vid WHERE t.tid = %d AND n.status = 1", $tid));
    if (!$nr_of_rows) {
      return;
    }
    $nid = db_result(db_query("SELECT n.nid FROM {term_node} t INNER JOIN {node} n ON n.vid = t.vid INNER JOIN {daily} d ON t.vid=d.vid WHERE t.tid = %d AND n.status = 1 ORDER BY d.date LIMIT 1 OFFSET %d", $tid, rand(0, $nr_of_rows - 1)));
    db_query("INSERT INTO {daily_random} (tid,nid,date) VALUES (%d,%d,'%s')", $tid, $nid, _daily_pack_date(_daily_today()));
  }

  $node = node_load($nid);
  return $node;
}


/**
 * Compute the data needed to display a month calendar with links.
 *
 * @param $tid
 *   Taxonomy term ID for this daily item collection.
 * @param
 *   $date The current date in date array format.
 * @return An array with the following elements:
 *   * For each day of this month, an element with the day number as key.
 *     Each array element is again an array, with a key 'weekday'
 *     containing the day of the week (sunday=0, saterday=6); with
 *     optionally a key 'link' containing the URL to link to (only if
 *     there is a daily item); and with optionally a key 'today' containing
 *     True if this is today; and a key 'future' which is True if the
 *     date lies in the future, and False if not.
 *   * An element with key 'next' and one with key 'prev'
 *     containing the URLs for the next and previous month; if there
 *     is no next or previous month, the corresponding element is not
 *     included.
 *   * An element 'date' containing the
 *     current date in system notation (seconds after Jan 1, 1980)
 *   So we have something like:
 *   @code
 *   $result[1]['weekday']  = 5
 *   $result[1]['link']     = 'daily/tid/4/20030101'
 *   $result[1]['today']    = True
 *   $result[1]['future']   = False
 *   .....
 *   $result['prev']        = 'daily/tid/4/20021231'
 *   $result['next']        = 'daily/tid/4/20030203'
 *   $result['date']        = 94758292343
 *   @endcode
 */
function _daily_calendar_month_data($tid, $date, $nid) {
  $result = array();
  $sql_extra = user_access('administer nodes')?"":" AND n.status = 1";

  // Get the date belonging to this nid. It may differ from $date if
  // we are viewing a revision. In that case, we do not want to see
  // this date.
  $original_date = db_result(db_query("SELECT d.date FROM {daily} d INNER JOIN {node} n ON n.vid = d.vid WHERE n.nid = %d", $nid));

  // Calculate weekdays and whether dates lie in the future.
  $start_of_month = mktime(0, 0, 0, $date["month"], 1, $date["year"]);
  $first_day = date("w", $start_of_month);
  $today_packed = _daily_pack_date(_daily_today());
  for ($day = 1; $day <= _daily_days_per_month($date["month"], $date["year"]); $day++) {
    $result[$day]["weekday"] = ($first_day + $day - 1) % 7;
    $this_day["year"] = $date["year"];
    $this_day["month"] = $date["month"];
    $this_day["day"] = $day;
    $result[$day]["future"] = _daily_pack_date($this_day) > $today_packed;
  }

  // Enter all dates in the current month which have items associated with them
  $som["year"] = $date["year"];
  $som["month"] = $date["month"];
  $som["day"] = 0;
  $eom["year"] = $date["year"];
  $eom["month"] = $date["month"];
  $eom["day"] = 99;
  $query = db_query("SELECT d.date,d.nid FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {daily} d ON n.vid = d.vid WHERE t.tid = %d AND d.date >= '%s' AND d.date < '%s' AND d.date <> '%s'". $sql_extra, $tid, _daily_pack_date($som), _daily_pack_date($eom), $original_date);
  while ($db_record = db_fetch_object($query)) {
    $found_date = _daily_unpack_date($db_record->date);
    $result[$found_date["day"]]["link"] = "node/". $db_record->nid;
  }

  // Mark the selected date, and enter the selected month.
  $result[$date["day"]]["today"] = True;
  $result["date"] = mktime(0, 0, 0, $date["month"], 15, $date["year"]);

  // Calculate the link to the previous month
  $query = db_query("SELECT d.nid FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {daily} d ON n.vid = d.vid WHERE t.tid = %d AND d.date < '%s' ". $sql_extra ." ORDER BY d.date DESC", $tid, _daily_pack_date($som));
  if ($db_record = db_fetch_object($query)) {
    $result['prev'] = "node/". $db_record->nid;
  }

  // Calculate the link to the next month
  $query = db_query("SELECT d.nid FROM {term_node} t INNER JOIN {node} n ON t.vid = n.vid INNER JOIN {daily} d ON n.vid = d.vid WHERE t.tid = %d AND d.date > '%s' ". $sql_extra ." ORDER BY d.date", $tid, _daily_pack_date($eom));
  if ($db_record = db_fetch_object($query)) {
    $result['next'] = "node/". $db_record->nid;
  }

  return $result;
}

/**
 * Get first/previous/next/last nids.
 *
 * Each daily item is part of a series, in which previous, next, 
 * first and last items can be found. This function looks those
 * up and puts them into an array.
 *
 * @param $node
 *   The node relative to which we are looking.
 * @return
 *   An array with keys 'first', 'prev', 'next' and 'last', each
 *   containing an object with fields date (in date array form) and nid.
 *   A node which is itself the last node does not get next and last
 *   items in this array.
 *   A node which is itself the first node does not get prev and first
 *   items in this array.
 *   An empty array is returned if the node is not found or does not
 *   have the right type.
 *   In addition, a key 'term_name' is generated containing the name of
 *   the term for which we have generated this data.
 *   So we get something like:
 *   @code
 *   $result['prev']['nid']  = 1032
 *   $result['prev']['date']['day'] = 10
 *   $result['prev']['date']['month'] = 5
 *   $result['prev']['date']['year'] = 2005
 *   $result['next']['nid']  = 1032
 *   $result['next']['date']['day'] = 10
 *   $result['next']['date']['month'] = 7
 *   $result['next']['date']['year'] = 2005
 *   .....
 *   $result['term_name'] = 'News'
 *   @endcode
 */
function _daily_get_prev_next($node) {
  $result = array();
  $sql_extra = user_access('adminster nodes')?"":" AND n.status = 1 ";

  $tids = _daily_find_our_tids($node->taxonomy, 'view');
  $query = db_query("SELECT d.nid, d.date FROM {daily} d INNER JOIN {node} n ON d.vid = n.vid INNER JOIN {term_node} t ON t.vid = n.vid WHERE t.tid = %d". $sql_extra ." ORDER BY d.date", $tids[0]);
  if ($db_record = db_fetch_object($query)) {
    if ($db_record->nid != $node->nid) {
      $db_record->date = _daily_unpack_date($db_record->date);
      $result['first'] = $db_record;
    }
  }
  else {
    // If we can't find any records, we may as well stop now.
    return $result;
  }

  $query = db_query("SELECT d.nid, d.date FROM {daily} d INNER JOIN {node} n ON d.vid = n.vid INNER JOIN {term_node} t ON t.vid= n.vid WHERE t.tid = %d". $sql_extra ." ORDER BY d.date DESC", $tids[0]);
  if ($db_record = db_fetch_object($query)) {
    if ($db_record->nid != $node->nid) {
      $db_record->date = _daily_unpack_date($db_record->date);
      $result['last'] = $db_record;
    }
  }

  $query = db_query("SELECT d.nid, d.date FROM {daily} d INNER JOIN {node} n ON d.vid = n.vid INNER JOIN {term_node} t ON n.vid = t.vid WHERE t.tid = %d AND d.date < '%s' ". $sql_extra ." ORDER BY d.date DESC", $tids[0], _daily_pack_date($node->daily_date));
  if ($db_record = db_fetch_object($query)) {
    $db_record->date = _daily_unpack_date($db_record->date);
    $result['prev'] = $db_record;
  }

  $query = db_query("SELECT d.nid, d.date FROM {daily} d INNER JOIN {node} n ON d.vid = n.vid INNER JOIN {term_node} t ON n.vid = t.vid WHERE t.tid = %d AND d.date > '%s'". $sql_extra ."  ORDER BY d.date", $tids[0], _daily_pack_date($node->daily_date));
  if ($db_record = db_fetch_object($query)) {
    $db_record->date = _daily_unpack_date($db_record->date);
    $result['next'] = $db_record;
  }

  $term = taxonomy_get_term($tids[0]);
  $result['term_name'] = $term->name;

  return $result;
}

/** @}
 */


/*******************
 * THEME FUNCTIONS *
 *******************/

/** @defgroup DailyTheme Theming 
 * Theming functions for calendar display. 
 *
 * @{
 */

/** @ingroup DailyHooks
 * Implementation of hook_theme()
 */
function daily_theme() {
  return array('daily_calendar_month' => array('arguments' => array('data')));
}

/**
 * Theme a month calendar.
 *
 * Theming is done by a combination of this function and the CSS sheet.
 * Normally, you do not have to overrule this function and you can
 * simply define your own CSS declarations.
 *
 * @param $data 
 *   The data as returned by _daily_calendar_month_data()
 * @return
 *   An HTML string which is ready to be printed.
 */
function theme_daily_calendar_month($data) {
  $var_first_day = variable_get("date_first_day", 0);
  $output = "<div class=\"daily-calendar\">\n";
  $output .= "<table summary=\"". t('A calendar to browse through this daily item collection') .".\">\n";
  $output .= "<tr><td colspan=\"7\" class=\"header-month\">". (array_key_exists('prev', $data)?l("&laquo;", $data["prev"], array('html' => TRUE)):"&nbsp;") ." ". format_date($data["date"], 'custom', "F Y") ." ".  (array_key_exists('next', $data)?l("&raquo;", $data["next"], array('html' => TRUE)):"&nbsp;") ."</td></tr>\n";

  $output .= "<tr class=\"header-week\">";
  $days = array(t('Su'), t('Mo'), t('Tu'), t('We'), t('Th'), t('Fr'), t('Sa'));
  for ($i = $var_first_day; $i < $var_first_day + 7; $i++)
    $output .= "<td>". $days[$i % 7] ."</td>";
  $output .= "</tr>\n";

  $first_day = ((1 - $data[1]["weekday"] + $var_first_day));
  while ($first_day > 1)
    $first_day -= 7;
  while ($first_day < -5)
    $first_day += 7;
  $dow = 0;
  for ($day = $first_day, $dow = 0; $day < 99; $day++, $dow = ($dow + 1) % 7) {
    if ($dow == 0 and $day >= 1 and !array_key_exists($day, $data)) {
      break;
    }
    if ($dow == 0) {
      $output .= "<tr class=\"row-week\">";
    }
    if (!array_key_exists($day, $data)) {
      $output .= "<td class=\"day-blank\"></td>";
    }
    else {
      $future = "";
      if ($data[$day]["future"]) {
        $future = " day-future";
      }
      if (array_key_exists("today", $data[$day])) {
        $output .= "<td class=\"day-today". $future ."\"><div>". $day ."</div></td>";
      }
      elseif (array_key_exists("link", $data[$day])) {
        $output .= "<td class=\"day-link". $future ."\">". l($day, $data[$day]["link"]) ."</td>";
      }
      else {
        $output .= "<td class=\"day-normal". $future ."\"><div>". $day ."</div></td>";
      }
    }
    if ($dow == 6) {
      $output .= "</tr>\n";
    }
  }
  $output .= "</table></div>\n";
  return $output;
}

/** @}
 */

# vim:sw=2:ft=php:et

?>
